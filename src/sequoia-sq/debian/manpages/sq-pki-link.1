.TH SQ 1 0.34.0 "Sequoia PGP" "User Commands"
.SH NAME
sq pki link \- Manage authenticated certificate and User ID links
.SH SYNOPSIS
.br
\fBsq pki link add\fR [\fIOPTIONS\fR] \fIFINGERPRINT|KEYID\fR \fIUSERID|EMAIL\fR
.br
\fBsq pki link retract\fR [\fIOPTIONS\fR] \fIFINGERPRINT|KEYID\fR \fIUSERID|EMAIL\fR
.br
\fBsq pki link list\fR [\fIOPTIONS\fR]  
.SH DESCRIPTION
Manage authenticated certificate and User ID links.
.PP
Link a certificate and User ID is one way of making `sq` consider a
binding to be authentic.  Another way is to use `sq pki certify` to
certify the binding with an explicitly configured trust root.  The
linking functionality is often easier to work with, and the
information is private by default.
.PP
Authenticated bindings can be used to designate a certificate using a
symbolic name.  For instance, using `sq encrypt`'s
`\-\-recipient\-userid` and `\-\-recipient\-email` options, a user can
designate a certificate using a User ID or an email address that is
authenticated for that certificate.
.PP
`sq` also uses authenticated certificates to authenticate other
data.  For instance, `sq verify` considers signatures made by an
authenticated certificate to be authentic.
.PP
Users can create a link using `sq pki link add`.  That link can later be
retracted using `sq pki link retract`.  A certificate can also be
accepted as a trusted introducer by passing the `\-\-ca` option to
`sq pki link add`.
.PP
`sq` implements linking using non\-exportable certifications, and an
implicit trust root.  An OpenPGP certificate directory, the default
certificate store used by `sq`, includes a local trust root, which
is stored under the `trust\-root` special name.  When the user
instructs `sq` to accept a binding, `sq` uses the local trust root
to create a non\-exportable certification, which it stores in the
certificate directory.  In this way, operations that use the Web of
Trust to authenticate a binding automatically use links.
.PP
When a user retracts a link, `sq` creates a new, non\-exportable
certification with zero trust.  This certification suppresses the
previous link.
.PP


.SH SUBCOMMANDS
.SS "sq pki link add"
Link a certificate and a User ID.
.PP
This cause `sq` to considers the certificate and User ID binding to be
authentic.
.PP
A certificate can also be accepted as a certification authority, which
is also known as a trusted introducer, by using the `\-\-ca` or
`\-\-depth` option.
.PP
A link can be retracted using `sq pki link retract`.
.PP
This command is similar to `sq pki certify`, but the certifications it
makes are done using the certificate directory's trust root, not an
arbitrary key.  Further, the certificates are marked as
non\-exportable.  The former makes it easier to manage certifications,
especially when the user's certification key is offline.  And the
latter improves the user's privacy, by reducing the chance that parts
of the user's social graph is leaked when a certificate is shared.
.PP
By default a link never expires.
Using the `\-\-expiry` argument specific validity periods may be defined.
It allows for providing a point in time for validity to end or a validity
duration.
.PP
`sq pki link` respects the reference time set by the top\-level `\-\-time`
argument. It sets the link's creation time to the reference time.
.PP


.SS "sq pki link retract"
Retract links.
.PP
This command retracts links that were previously created using `sq
pki link add`.  See that subcommand's documentation for more details.
Note: this is called `retract` and not `remove`, because the
certifications are not removed.  Instead a new certification is added,
which says that the binding has not been authenticated.
.PP
`sq pki link retract` respects the reference time set by the top\-level
`\-\-time` argument.  This causes a link to be retracted as of a
particular time instead of the current time.
.PP

.SS "sq pki link list"
List links.
.PP
This command lists all bindings that are linked or whose link has been
retracted.
.PP

.SH EXAMPLES
.SS "sq pki link add"
.PP

.PP
The user links 0123456789ABCDEF and the User ID
\&'<romeo@example.org>'.
.PP
.nf
.RS
sq pki link add 0123456789ABCDEF '<romeo@example.org>'
.RE
.PP
.fi

.PP
The user examines 0123456789ABCDEF and then accepts the certificate
0123456789ABCDEF with its current set of self\-signed User IDs.
.PP
.nf
.RS
sq cert export \-\-cert 0123456789ABCDEF | sq inspect
.RE
.PP
.fi
\&...
.PP
.nf
.RS
sq pki link add 0123456789ABCDEF \-\-all
.RE
.PP
.fi

.PP
The user links the certificate and its current self\-signed User
IDs for a week.
.PP
.nf
.RS
sq pki link add \-\-expires\-in 1w 0123456789ABCDEF \-\-all
.RE
.PP
.fi

.PP
The user accepts the certificate, and its current self\-signed User
IDs as a certification authority.  That is, the certificate is
considered a trust root.
.PP
.nf
.RS
sq pki link add \-\-ca '*' 0123456789ABCDEF \-\-all
.RE
.PP
.fi

.PP
The user accepts the certificate and its current self\-signed User
IDs as a partially trusted certification authority.
.PP
.nf
.RS
sq pki link add \-\-ca '*' \-\-amount 60 0123456789ABCDEF \-\-all
.RE
.PP
.fi

.PP
The user retracts their acceptance of 0123456789ABCDEF and any
associated User IDs.  This effectively invalidates any links.
.PP
.nf
.RS
sq pki link retract 0123456789ABCDEF
.RE
.fi
.PP
.SH "SEE ALSO"
.nh
\fBsq\fR(1), \fBsq\-pki\fR(1), \fBsq\-pki\-link\-add\fR(1), \fBsq\-pki\-link\-retract\fR(1), \fBsq\-pki\-link\-list\fR(1).
.hy
.PP
For the full documentation see <https://book.sequoia\-pgp.org>.
.SH VERSION
0.34.0 (sequoia\-openpgp 1.19.0)
